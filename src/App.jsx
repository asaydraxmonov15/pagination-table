import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Class from './components/class/Class'
import Function from './components/function/Function'
import './Style.css'
const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Function />} />
        <Route path="/class-based" element={<Class />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
