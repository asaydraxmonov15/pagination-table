import React, { useState } from 'react'

const Comments = ({ comments }) => {
  const [search, setSearch] = useState('')

  return (
    <div>
      <input
        type="text"
        placeholder="Search by Name or Body..."
        className=""
        onChange={(e) => setSearch(e.target.value)}
        value={search}
      />
      <section>
        <table>
          <thead>
            <tr>
              <th>Id</th>
              <th>PostId</th>
              <th>Name</th>
              <th>Email</th>
              <th>Body</th>
            </tr>
          </thead>
          <tbody>
            {comments
              .filter((comment) => {
                if (search == '') {
                  return comment
                }

                if (
                  comment.name.toLowerCase().includes(search.toLowerCase()) ||
                  comment.body.toLowerCase().includes(search.toLowerCase())
                ) {
                  return comment
                }
              })
              .map((comment) => {
                const { postId, id, name, email, body } = comment
                return (
                  <tr key={id}>
                    <td>{id}</td>
                    <td>{postId}</td>
                    <td>{name}</td>
                    <td>{email}</td>
                    <td>{body}</td>
                  </tr>
                )
              })}
          </tbody>
        </table>
      </section>
    </div>
  )
}

export default Comments
