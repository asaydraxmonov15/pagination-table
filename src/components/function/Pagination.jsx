import React from 'react'

const Pagination = ({
  commentsPerPage,
  totalComments,
  currentPage,
  setCurrentPage,
}) => {
  const pageNumbers = []

  for (let i = 1; i <= Math.ceil(totalComments / commentsPerPage); i++) {
    pageNumbers.push(i)
  }

  return (
    <main className='page-buttons'>
      {pageNumbers.map((number, index) => {
        return (
          <button
            key={index}
            className={`page-button ${
              index + 1 === currentPage ? 'active-button' : null
            }`}
            onClick={() => setCurrentPage(index + 1)}

          >
            {number}
          </button>
        )
      })}
    </main>
  )
}

export default Pagination
