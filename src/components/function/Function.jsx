import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import Comments from './Comments'
import Pagination from './Pagination'

const Function = () => {
  const [comments, setComments] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [commentsPerPage] = useState(10)

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/comments')
      .then((res) => res.json())
      .then((data) => {
        setComments(data)
      })
  }, [])

  const indexOfLastComment = currentPage * commentsPerPage
  const indexOfFirstComment = indexOfLastComment - commentsPerPage
  const currentComment = comments.slice(indexOfFirstComment, indexOfLastComment)

  const nextPage = () => {
    setCurrentPage((oldPage) => {
      let nextPage = oldPage + 1
      if (nextPage > comments.length / commentsPerPage) {
        nextPage = 1
      }
      return nextPage
    })
  }

  const prevPage = () => {
    setCurrentPage((oldPage) => {
      let prevPage = oldPage - 1
      if (prevPage < 1) {
        prevPage = comments.length / commentsPerPage
      }
      return prevPage
    })
  }

  return (
    <div className="container">
      <h1 className="title">Function Example</h1>
      <Link to="/class-based">Go to Class Example</Link>
      <div>
        <Comments comments={currentComment} />
        <div className="buttons">
          <button onClick={prevPage}>Prev</button>
          <Pagination
            commentsPerPage={commentsPerPage}
            totalComments={comments.length}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
          />
          <button onClick={nextPage}>Next</button>
        </div>
      </div>
    </div>
  )
}

export default Function
