import React from 'react'

class ClassPagination extends React.Component {
  render() {
    let pageNumbers = []
    for (
      let i = 1;
      i <= Math.ceil(this.props.totalComments / this.props.commentsPerPage);
      i++
    ) {
      pageNumbers.push(i)
    }

    return (
      <main className="page-buttons">
        {pageNumbers.map((number, index) => {
          return (
            <button
              key={index}
              className={`page-button ${
                index + 1 === this.props.currentPage ? 'active-button' : null
              }`}
              onClick={() => this.props.setCurrentPage(index + 1)}
            >
              {number}
            </button>
          )
        })}
      </main>
    )
  }
}

export default ClassPagination
