import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ClassComments from './ClassComments'
import ClassPagination from './ClassPagination'

class Class extends Component {
  constructor(props) {
    super(props)
    this.state = {
      comments: [],
      currentPage: 1,
      commentsPerPage: 10,
    }

    this.nextPage = this.nextPage.bind(this)
    this.prevPage = this.prevPage.bind(this)
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/comments')
      .then((res) => res.json())
      .then((data) => {
        this.setState({ comments: data })
      })
  }

  nextPage = () => {
    this.setState((state) => {
      let nextPage = state.currentPage + 1
      if (nextPage > this.state.comments / this.state.commentsPerPage) {
        nextPage = 1
      }
      return { currentPage: nextPage }
    })
  }

  prevPage = () => {
    this.setState((state) => {
      let prevPage = state.currentPage - 1
      if (prevPage < 1) {
        prevPage = this.state.comments / this.state.commentsPerPage
      }
      return { currentPage: prevPage }
    })
  }

  render() {
    const indexOfLastComment =
      this.state.currentPage * this.state.commentsPerPage
    const indexOfFirstComment = indexOfLastComment - this.state.commentsPerPage

    const currentComment = this.state.comments.slice(
      indexOfFirstComment,
      indexOfLastComment,
    )

    return (
      <div className="container">
        <h1 className="title">Class Example</h1>
        <Link to="/">Go to Function Example</Link>
        <div>
          <ClassComments comments={currentComment} />
          <div className="buttons">
            <button onClick={this.prevPage}>Prev</button>
            <ClassPagination
              commentsPerPage={this.state.commentsPerPage}
              totalComments={this.state.comments.length}
              currentPage={this.state.currentPage}
              setCurrentPage={(currentPage) => this.setState({ currentPage })}
            />
            <button onClick={this.nextPage}>Next</button>
          </div>
        </div>
      </div>
    )
  }
}

export default Class
