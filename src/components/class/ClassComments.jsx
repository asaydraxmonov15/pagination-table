import React, { Component } from 'react'

class ClassComments extends Component {
  constructor(props) {
    super(props)
    this.state = {
      search: '',
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange = (e) => {
    const value = e.target.value
    this.setState({search: value})
  }

  render() {
    return (
      <div>
        <input
          type="text"
          placeholder="Search by Name or Body..."
          className=""
          onChange={this.handleChange}
          value={this.state.search}
        />
        <section>
          <table>
            <thead>
              <tr>
                <th>Id</th>
                <th>PostId</th>
                <th>Name</th>
                <th>Email</th>
                <th>Body</th>
              </tr>
            </thead>
            <tbody>
              {this.props.comments
                .filter((comment) => {
                  if (this.state.search == '') {
                    return comment
                  }

                  if (
                    comment.name.toLowerCase().includes(this.state.search.toLowerCase()) ||
                    comment.body.toLowerCase().includes(this.state.search.toLowerCase())
                  ) {
                    return comment
                  }
                })
                .map((comment) => {
                  const { postId, id, name, email, body } = comment
                  return (
                    <tr key={id}>
                      <td>{id}</td>
                      <td>{postId}</td>
                      <td>{name}</td>
                      <td>{email}</td>
                      <td>{body}</td>
                    </tr>
                  )
                })}
            </tbody>
          </table>
        </section>
      </div>
    )
  }
}

export default ClassComments
